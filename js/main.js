function checkHELLO(field, rules, i, options){
	if (field.val() != "HELLO") {
		return options.allrules.validate2fields.alertText;
	}
}


$( document ).ready(function() {
	
	var config = {
			'.chosen-fonction' : {disable_search:false,
			width: '100%',
			no_results_text: "Aucun résultat ne correspond",
			placeholder_text_single: "Choisissez une fonction"},
		}
		for (var selector in config) {
			$(selector).chosen(config[selector]);
		}
	
	jQuery("#form_validate").validationEngine({scroll: false, promptPosition : "topRight:-130", prettySelect : true, useSuffix: "_chosen"});

	var click_div1=0;
	var click_div2=0;
	var click_div3=0;
	var precedent='nul';
	
	function slide_div(click_div1,click_div2,click_div3,precedent){
		var selecteur='';
		if (click_div1==1) {selecteur='#sousmenuServices';}
		if (click_div2==1) {selecteur='#sousmenuSecteurs';}
		if (click_div3==1) {selecteur='#sousmenuCarrieres';}
		
		if (precedent!='nul'){
		$( precedent ).slideToggle( "slow",function(){
			$( selecteur ).slideToggle( "slow");
		});
		} else if (precedent=='nul') {
			$( selecteur ).slideToggle( "slow");
		}	
	}
	
	
	
	$("#services" ).click(function() {
	
		var click_div1=1;var click_div2=0;var click_div3=0;
		if (precedent!='#sousmenuServices') {
			slide_div(click_div1,click_div2,click_div3,precedent);
			precedent='#sousmenuServices';
		}	else if (precedent == '#sousmenuServices') {
			$( '#sousmenuServices' ).slideToggle( "slow");
			precedent='nul';
		}
		
	});
	
	$("#secteurs" ).click(function() {
	
		var click_div1=0;var click_div2=1;var click_div3=0;
		if (precedent!='#sousmenuSecteurs') {
			slide_div(click_div1,click_div2,click_div3,precedent);
			precedent='#sousmenuSecteurs';
		}	else if (precedent == '#sousmenuSecteurs') {
			$( '#sousmenuSecteurs' ).slideToggle( "slow");
			precedent='nul';
		}
		
	});
	
	$("#carrieres" ).click(function() {
	
		var click_div1=0;var click_div2=0;var click_div3=1;
		if (precedent!='#sousmenuCarrieres') {
			slide_div(click_div1,click_div2,click_div3,precedent);
			precedent='#sousmenuCarrieres';
		}	else if (precedent == '#sousmenuCarrieres') {
			$( '#sousmenuCarrieres' ).slideToggle( "slow");
			precedent='nul';
		}
		
	});
	
	$("#global" ).click(function() {
	
		if (precedent!='nul') {
			$(precedent).slideUp("slow");
			precedent='nul';
		}
		
	});
	
	$("#return_form" ).click(function() {
	
		$("#success").fadeTo( "slow", 0 );
		$("#success").hide();
		$("#form_validate").fadeTo( "slow", 1 );
		$("#form_validate").show();
		grecaptcha.reset();
		$("#linkedin_af_btn").show();
		$("#linkedin_af_btn").fadeTo( "slow", 1 );
		
	});
	
	$("#btn_close_viewer" ).click(function() {
	
		$("#pdf_viewer").fadeTo( "slow", 0 );
		$("#pdf_viewer").hide();
		document.body.style.overflow = 'auto';
		
	});
	
	$("#reopen_doc" ).click(function() {
	
		jQuery('html, body').animate(
			{scrollTop:0}, 
			{
				//easing: 'swing', 
				duration: 'fast',
				complete:  function() { 
				$("#pdf_viewer").show(); 
				$("#pdf_viewer").fadeTo( "slow", 1 ); 
				document.body.style.overflow = 'hidden'; 
			} 
		});	
	});
	
	$(".reopen_doc_icon img" ).click(function() {
	
		jQuery('html, body').animate(
			{scrollTop:0}, 
			{
				//easing: 'swing', 
				duration: 'fast',
				complete:  function() { 
				$("#pdf_viewer").show(); 
				$("#pdf_viewer").fadeTo( "slow", 1 ); 
				document.body.style.overflow = 'hidden'; 
			} 
		});	
	});

	
	 $("#envoyer").hover(function () {
    	$(".download_icon img").toggleClass("anim_download_icon");
	 });
	
	 $(".download_icon img").hover(function () {
    	$("#envoyer").toggleClass("anim_download_btn");
	 });
	 
	 $("#reopen_doc").hover(function () {
    	$(".reopen_doc_icon img").toggleClass("anim_reopen_doc_icon");
	 });
	
	 $(".reopen_doc_icon img").hover(function () {
    	$("#reopen_doc").toggleClass("btn_form_hover");
	 });
});


function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // IE 12 => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

function send_form() {	
	
	if ( $('#form_validate').validationEngine('validate') ) {
		if ($("#g-recaptcha-response").val()) {
			dataLayer.push({'event': 'form-validation'});
			$( "button:last" ).trigger( "click" );
			$('#error_captcha').html('');
			$('#error_captcha').hide();
			$("#error_captcha").fadeTo( "slow", 0 );
			$('#loading_form').show();
			$('#loading_form').html('<img src="img/deloitte_00_loading.GIF" />');
			$("#form_validate :submit").hide();
			$.ajax({ 
			   type: "POST", 
			   url: "save.php",
			   data: "g-recaptcha-response="+$("#g-recaptcha-response").val()+"&prenom="+encodeURIComponent($("#prenom").val())+"&nom="+encodeURIComponent($("#nom").val())+"&email="+encodeURIComponent($("#email").val())+"&organisation="+encodeURIComponent($("#organisation").val())+"&fonction="+encodeURIComponent($("#fonction").val())+"&telephone="+encodeURIComponent($("#telephone").val()), 
			   success: function(msg){ 
					if(msg==2) 
					{	
						$("#form_validate").fadeTo( "slow", 0 );
						
						
						$("#success").show();
						$("#success").fadeTo( "slow", 1 );
						$('#loading_form').html('');
						$("#form_validate :submit").show();
						$('#form_validate').trigger("reset");
						
						$("#form_validate").hide();
						
						var ie_ver = detectIE();
						
						if ((ie_ver > 8) || (ie_ver==false)) {
							jQuery('html, body').animate(
								{scrollTop:0}, 
								{
									//easing: 'swing', 
									duration: 'fast',
									complete:  function() { 
										$("#pdf_viewer").show(); 
										$("#pdf_viewer").fadeTo( "slow", 1 ); 
										document.body.style.overflow = 'hidden'; 
										$("#linkedin_af_btn").fadeTo( "slow", 0 );
										$("#linkedin_af_btn").hide();
								} 
							});
						} else {
							window.location.href = "etude.php";
						}
					}
					else if(msg==1)
					{
						$('#loading_form').html('');
						$('#loading_form').hide();
						$("#form_validate :submit").show();
						$('#error_captcha').html('<p>Une erreur est survenu lors de l’enregistrement de votre demande, veuillez réessayer</p>');
						$('#error_captcha').show();
						$("#error_captcha").fadeTo( "slow", 1 );
						
					} else {
						$('#loading_form').html('');
						$('#loading_form').hide();
						$("#form_validate :submit").show();
						$('#error_captcha').html('<p>Toute tentative de spam est interdite</p>');
						$('#error_captcha').show();
						$("#error_captcha").fadeTo( "slow", 1 );
					}
			   }
			});
		} else {
			$('#error_captcha').html('<p>Veuillez cocher la case « Je ne suis pas un robot » </p>');
			$('#error_captcha').show();
			$("#error_captcha").fadeTo( "slow", 1 );
		}
	} else {
		   
	}
		
	return false; 
}
