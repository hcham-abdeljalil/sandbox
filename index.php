<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="css/styles.css" />	
<meta name="robots" content="noindex, nofollow">
<link rel="shortcut icon" href="https://www2.deloitte.com/etc/designs/dcom/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="https://www2.deloitte.com/etc/designs/dcom/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="apple-touch-icon" href="https://www2.deloitte.com/etc/designs/dcom/assets/images/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://www2.deloitte.com/etc/designs/dcom/assets/images/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://www2.deloitte.com/etc/designs/dcom/assets/images/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://www2.deloitte.com/etc/designs/dcom/assets/images/touch-icon-ipad-retina.png">

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<link rel="stylesheet" href="plugins/chosen/chosen.css">
<script src="plugins/chosen/chosen.jquery.js" type="text/javascript"></script>

<link rel="stylesheet" href="plugins/validation_engine/validationEngine.jquery.css" type="text/css"/>
<script src="plugins/validation_engine/languages/jquery.validationEngine-fr.js" type="text/javascript" charset="utf-8"></script>
<script src="plugins/validation_engine/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>

  <script>
    dataLayer = [{
      'download-label': 'nom etude pilote'
          }];
  </script>


<meta name="robots" content="noindex, nofollow">
<title>Deloitte France | Formulaire</title>        

</head>

<body>

<!-- Header -->
<div id="header">
    <!--Div Global Header -->
	<div id="global-header">
		<!-- First Logo -->
		<div id="logo">
		<a href="https://www2.deloitte.com/fr/fr.html">
		<img src="img/deloitte-logo.png"/></a>
		</div>
		<!-- End Logo -->
			    
		<!-- First Search -->
       <div id="search">
               	 <form id="searchform" name="searchform" action="http://www2.deloitte.com/fr/fr/misc/search.html">
					<input type="text" id="q" name="qr" value="" autocomplete="off" placeholder="Rechercher">
                    <input type="submit" value=" " id="submit">
                 </form>
        </div>
     	<!-- End Search -->
				
		<!-- First Menu -->
	   <div id="navigation">
			<ul>
			<li>
			<a href="#" id="services">Services</a>
			</li>
			<li>
			<a href="#" id="secteurs">Secteurs</a>
			</li>
			<li>
			<a href="#" id="carrieres">Carrières</a>
			</li>
			</ul>
		</div>
		<!-- End Menu-->
		<!-- Clear Float -->
		<div class="clear"> </div>
		<!-- End Clear Float -->
		<!-- First Sub Menu -->
		<div id="sousmenuCarrieres">
			<ul>
			<li><a href="http://www.deloitterecrute.fr/?icid=top_vivre-deloitte" target="_blank">Vivre Deloitte</a></li>
			<li><a href="http://www.deloitterecrute.fr/postuler?icid=top_postuler" target="_blank">Postuler</a></li>
			</ul>
		</div>
		<div id="sousmenuSecteurs">
			<ul>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/associations-et-fondations.html?icid=top_associations-et-fondations" target="_blank">Associations et fondations</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/cb.html?icid=top_cb" target="_blank">Consumer Business</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/energie-et-ressources.html?icid=top_energie-et-ressources" target="_blank">Energie & ressources</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/immobilier.html?icid=top_immobilier" target="_blank">Immobilier</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/manufacturing.html?icid=top_manufacturing" target="_blank">Manufacturing</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/sante-et-sciences-de-la-vie.html?icid=top_sante-et-sciences-de-la-vie" target="_blank">Santé & Science de la vie</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/secteur-public.html?icid=top_secteur-public" target="_blank">Secteur public</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/services-financiers.html?icid=top_services-financiers" target="_blank">Services financiers</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/secteurs/tmt.html?icid=top_tmt" target="_blank">Technologies, Médias & Télécommunications</a></li>
			</ul>
		</div>
		<div id="sousmenuServices">
			<ul>
			<li><a href="http://www2.deloitte.com/fr/fr/services/audit.html?icid=top_audit" target="_blank">Audit</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/finance.html?icid=top_finance" target="_blank">Finance</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/fiscalite.html?icid=top_fiscalite" target="_blank">Fiscalité</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/fusions-acquisitions.html?icid=top_fusions-acquisitions" target="_blank">Fusions-Acquisitions</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/juridique.html?icid=top_juridique" target="_blank">Juridique</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/marketing-et-commercial.html?icid=top_marketing-et-commercial" target="_blank">Marketing & Commercial</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/risque-compliance-et-controle-interne.html?icid=top_risque-compliance-et-controle-interne" target="_blank">Risque, Compliance & Contrôle interne</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/strategie-et-innovation.html?icid=top_strategie-et-innovation" target="_blank">Stratégie & Innovation</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/supply-chain-et-achats.html?icid=top_supply-chain-et-achats" target="_blank">Supply Chain & Achats</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/systeme-information-et-technologie.html?icid=top_systeme-information-et-technologie" target="_blank">Système d'information & Technologie</a></li>
			<li><a href="http://www2.deloitte.com/fr/fr/services/talents-et-ressources-humaines.html?icid=top_talents-et-ressources-humaines" target="_blank">Talents & Ressources Humaines</a></li>
			</ul>
		</div>
		<!-- End SubMenu -->
	</div>
	<!--End Div Global Header -->
</div>
<!-- End Header --> 

<div id="global">
	<!-- Global Center-->
	<div id="global-center">
	        <!-- Main-->
			<div id="main">
			            <!-- Banner Image and Cartouche -->
				        <div id="banner">
                          <img src="img/formation-visuel.jpg" alt="formulaire-contact-specifique-formation.png"  />
                          <div id="blocContact">Téléchargement</div>
                        </div>
						 <!-- End Banner Image and Cartouche -->
						 <!-- Texte Description -->
					    <div id="header-description">
						<h1>Lorem ipsum dolor sit amet</h1> <!--Titre -->
                        <h2>Aliquam imperdiet fermentum purus in sagittis</h2> <!--Sous Titre -->
				       	</div>
						<!-- End Texte Description -->						
	        </div>    
			<!-- End Main-->
             
			<!-- Content  Form -->
	        <div class="center-content-simple">
            <div id="linkedin_af_btn">
            
				<script src="//platform.linkedin.com/in.js" type="text/javascript">
                  api_key: 77azpaoizwowwp
                  noAuth: true
                  lang: fr_FR
                </script>
                <script type="IN/Form" data-form="form_validate" data-field-firstname="prenom" data-field-lastname="nom" data-field-phone="telephone" data-field-email="email" data-field-company="organisation"></script>
            	
            </div>
            
                <!-- The Form -->
			  <form action="javascript:void(0)" method="post" id="form_validate" style="z-index:9999;" name="form_validate">
				
                <div id="divleft">
                     <label for="prenom">Prénom </label>
                     <input  type="text" id="prenom" name="prenom" placeholder="Prénom" size="35"  class="validate[required] text-input">
					 <label for="nom">Nom </label>
					 <input  type="text"id="nom" name="nom" placeholder="Nom" size="35"  class="validate[required] text-input">
				   	 <label for="email">Email </label>
                     <input  type="text" id="email" name="email" placeholder="Email" size="35"  class="validate[required,custom[email]] text-input">
                </div>
                <div id="divright">
					 <label for="organisation">Organisation </label>
                     <input  type="text" id="organisation" name="organisation" placeholder="Organisation" size="35"  class="validate[required] text-input">
            
					 <label for="fonction">Fonction </label>
                     <select name="fonction" id="fonction" class="chosen-fonction validate[required]" data-placeholder="" > 
                            <option value=""></option> 
                            <option value="Autres">Autres</option>
                        	<option value="Achats">Achats</option>
                            <option value="Association">Association</option>
                            <option value="Comité de direction">Comité de direction</option>
                            <option value="Commerciale">Commerciale</option>
                            <option value="Communication">Communication</option>
                            <option value="Deloitte">Deloitte</option>
                            <option value="Direction Générale">Direction Générale</option>
                            <option value="Direction Opérations">Direction Opérations</option>
                            <option value="Environnement Développement Durable">Environnement Développement Durable</option>
                            <option value="Finance & Gestion">Finance &amp; Gestion</option>
                            
                            <option value="Fonction Publique">Fonction Publique</option>
                            <option value="Gouvernance">Gouvernance</option>
                            <option value="Immobilier">Immobilier</option>
                            
                            
                            <option value="Industrie Production">Industrie Production</option>
                            <option value="Juridique & Fiscal">Juridique &amp; Fiscal</option>
                            <option value="Logistique">Logistique</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Métiers Banque">Métiers Banque</option>
                            
                            
                            <option value="Métiers du Conseil">Métiers du Conseil</option>
                            <option value="Qualité & technique">Qualité &amp; technique</option>
                            <option value="R&D">R&amp;D</option>
                            <option value="Ressources Humaines">Ressources Humaines</option>
                            <option value="Retraite">Retraite</option>
                            
                            
                             <option value="Services Généraux">Services Généraux</option>
                            <option value="Stratégie & Investissement">Stratégie &amp; Investissement</option>
                            <option value="Systèmes Information">Systèmes Information</option> 
                            
                        </select>
                        
					 <label for="organisation">Téléphone </label>
                     <input  type="text" id="telephone" name="telephone" placeholder="Téléphone" size="35"  class="text-input">
                     
                </div>
                <div class="clear"></div>
                
                <div class="form_rightcol">
                	<center><div class="g-recaptcha" data-sitekey="6Ldjyw0TAAAAAEOH6B2_6Sn9OmZvByM-UDzExU2E"></div></center>
                    <div class="clear"></div>
                    
                	<div id="error_captcha"></div>	
                	<div id="loading_form"></div>
                    	
                    	<input type="submit"  name="Submit" id="envoyer" class="envoyer" value="Télécharger"  onClick="send_form()">
                    
                    <div id="cnil-form"><p><i><br/>« Les informations recueillies par Deloitte SAS sont utilisées uniquement dans le cadre légal prévu en France pour le respect de la vie privée. La société Deloitte SAS est le destinataire des données. Conformément à la loi «informatique et libertés» n°78-17 du 6 janvier 1978 modifiée en 2004, vous bénéficiez d’un droit d’accès et de rectification des informations qui vous concernent, que vous pouvez exercer en vous adressant à : <a href="mailto:frcommunicationdigitale@deloitte.fr">frcommunicationdigitale@deloitte.fr</a>. Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant. »</i></p></div>

			    </div> 
			   </form>
             
		       <!-- End Form -->
               <div id="success">
                    
               		<h1>Merci de votre intérêt pour nos sujets.</h1>
                    
                    	<p><a class="btn_form" id="reopen_doc">Ouvrir l'étude</a></p>
                    
               </div>
               <!-- End success -->
               
            </div>
            <!-- End Content -->
   
    </div> 
	<!-- End Global Center-->
     <div class="clear"></div>
</div>
<!-- End Global-->


	<!-- Global Footer -->
	<div id="global-footer">
        <div class="ft1">
            <p class="footer-titre">Nous connaître</p>
			<ul class="footer-ligne">
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/deloitte-en-bref.html">Deloitte en France</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/deloitte-en-regions.html">Deloitte en régions</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/.html" target="_blank">Technology Fast 50</a></li>
				<li> <a href="http://www2.deloitte.com/fr/fr/footerlinks/international-servicesgroup.html">International Services Group</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/deloitte-afrique-francophone.html">Deloitte Afrique francophone</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/pages/sustainability-services/topics/deloitte-sustainability-services.html?icid=bottom_deloitte-sustainability-services">Deloitte Sustainability Services</a></li>
                <li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/pressreleasespage.html?q=*&sp_x_18=content-type&sp_q_18=News&sp_s=date-published">Communiqués de presse</a></li>
				<li class=""><a href="http://dviews.deloitte-france.fr/" target="_blank">Blog</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/reseaux-sociaux.html">Réseaux sociaux</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/office-locator.html">Localisez nos bureaux</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/submit-rfp.html">Faire appel à nos services</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr.html">Home</a></li>
				<li class=""><a href="https://webmail.deloitte.fr/">Messagerie collaborateurs</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/nous-contacter.html">Nous contacter</a></li>
				<li class=""><a href="http://www2.deloitte.com/fr/fr/footerlinks/contact-us.html">Formulaire de contact</a></li>
			</ul> 
        </div>
        <div class="ft">            
            <p class="footer-titre">Services</p>
			<ul class="footer-ligne">
                <li><a href="http://www2.deloitte.com/fr/fr/services/audit.html">Audit</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/finance.html">Finance</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/fiscalite.html">Fiscalité</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/fusions-acquisitions.html">Fusions-Acquisitions</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/juridique.html">Juridique</a></li> 
                <li><a href="http://www2.deloitte.com/fr/fr/services/marketing-et-commercial.html">Marketing &amp; Commercial</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/risque-compliance-et-controle-interne.html">Risque, Compliance & Contrôle interne</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/strategie-et-innovation.html">Stratégie & Innovation</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/supply-chain-et-achats.html">Supply Chain & Achats</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/systeme-information-et-technologie.html">Système d’information & Technologie</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/services/talents-et-ressources-humaines.html">Talents & Ressources Humaines</a></li>
			</ul>
        </div>
        <div class="ft">
            <p class="footer-titre">Secteurs</p>
			<ul class="footer-ligne">
				<li><a href="http://www2.deloitte.com/fr/fr/secteurs/associations-et-fondations.html">Associations et fondations</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/cb.html">Consumer Business</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/energie-et-ressources.html">Energie &amp; ressources</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/immobilier.html">Immobilier</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/manufacturing.html">Manufacturing / Automotive</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/sante-et-sciences-de-la-vie.html">Santé & Sciences de la vie</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/secteur-public.html">Secteur public</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/services-financiers.html">Services financiers</a></li>
                <li><a href="http://www2.deloitte.com/fr/fr/secteurs/tmt.html">Technologies, Médias &amp; Télécommunications</a></li>
			</ul> 
        </div>
            
         <div class="ft">
            <p class="footer-titre">Carrières</p>
			<ul class="footer-ligne">
				<li><a href="http://www.deloitterecrute.fr/" target="_blank">Vivre Deloitte</a></li>
				<li><a href="http://www.deloitterecrute.fr/postuler" target="_blank">Postuler</a></li>
				<li style="visibility:hidden;"><a href="http://www.deloitterecrute.fr/" target="_blank">Vivre Deloitte</a></li>
				<li style="visibility:hidden;"><a href="http://www.deloitterecrute.fr/postuler" target="_blank">Postuler</a></li>
                 
			</ul> 
         </div>
         <div class="ft">
            <p class="footer-titre">En savoir plus</p>
			<ul class="footer-ligne">
				<li class="footer-contact-us"><a href="https://www2.deloitte.com/fr/fr/legal/a-propos.html">A propos</a></li>
                <li class="footer-contact-us"><a href="https://www2.deloitte.com/fr/fr/legal/legal.html">Conditions d'Utilisation</a></li>
                <li class="footer-contact-us"><a href="https://www2.deloitte.com/fr/fr/legal/cookies.html">Cookies</a></li>
                <li class="footer-contact-us"><a href="https://www2.deloitte.com/fr/fr/legal/privacy.html">Politique de confidentialité</a></li>
			</ul> 
         </div>
         <div style="clear:both;"></div>
         <div id="cnil">
		    <p>© <?php echo date('Y'); ?> Deloitte SAS. Voir les <a href="http://www2.deloitte.com/fr/fr/legal/legal.html">conditions générales d'utilisation</a></p>
 		    <p>Deloitte fait référence à un ou plusieurs cabinets membres de Deloitte Touche Tohmatsu Limited, société de droit anglais (« private company limited by guarantee »), et à son réseau de cabinets membres constitués en entités indépendantes et juridiquement distinctes. 
             Pour en savoir plus sur la structure légale de Deloitte Touche Tohmatsu Limited et de ses cabinets membres, consulter <a href="http://www.deloitte.com/about">www.deloitte.com/about</a>. En France, Deloitte SAS est le cabinet membre de Deloitte Touche Tohmatsu Limited, et les services professionnels sont rendus par ses filiales et ses affiliés.
            </p>
         </div>
	</div>
    <!-- End Global Footer -->
    
    <div id="pdf_viewer">
		<iframe id="myiframe" src="http://www.deloitte-france.fr/sandbox/pdf-viewer/web/viewer.html?file=deloitte_big-data-et-analytics-industrie-automobile_sept-2015.pdf"></iframe>
        <a id="btn_close_viewer">Fermer</a>
	</div>
    
<script src="js/main.js" type="text/javascript" charset="utf-8"></script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P5XL9S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P5XL9S');</script>
<!-- End Google Tag Manager -->


</body>
</html>