<?php

include("config.php"); 
/////////////////////////////// Functions ///////////////////////////

function clean_field($field)
{
	$field= htmlentities($field, ENT_QUOTES, "UTF-8");
	$field=str_replace("'", "\'", $field);
	
	return $field;
}

/**************************************************************/
function edit_table($connect,$table,$fields,$values,$type='',$id='') {
	
	$field_by_table='';
	$fields_string = '';
	$values_string = "";
	$num_fields = count($fields);
	$num_values = count($values);
	$i = 0;
	$j = 0;
	switch ($type) {
		default:
			
			foreach ($fields as $field) {
				if(++$i === $num_fields) {
					$fields_string .= $field;
				} else {
					$fields_string .= $field.',';
				}
			}
			
			foreach ($values as $value) {
				if(++$j === $num_values) {
					$values_string .= "'".$value."'";
				} else {
					$values_string .= "'".$value."',";
				}
			}
			
			$sql="INSERT INTO ".$table." (".$fields_string.") VALUES (".$values_string.")";
			$req = mysqli_query($connect,$sql) or die('Erreur SQL !<br />'.$sql.'<br />'.mysqli_error($connect));
			break;
	}
	
	unset($num_fields);
	unset($values_string);
}
/////////////////////////////// Mails ///////////////////////////

function push_mail($connect,$contacts,$subject,$titre,$soustitre,$message,$bcc='',$type='') {
	
	$i = 0;
	
	$mailing_list='';
	
	$from ='dcom-form@deloitte.fr';
		
	$num_contacts=count($contacts);
	
	foreach ($contacts as $contact) {
		if(++$i === $num_contacts) {
			$mailing_list .= $contact;
		} else {
			$mailing_list .= $contact.';';
		}
	}
	
	$subject =  utf8_decode($subject);
	$subject = mb_encode_mimeheader($subject);
	
	$headers  = "MIME-Version: 1.0 \n";
	$headers .= "Content-Transfer-Encoding: 8bit \n";
	$headers .= "Content-type: text/html; charset=utf-8 \n";
	$headers .= "From: ".$from."\r\n" ;
	$headers .= "Reply-To: ".$from."\r\n" ;
	$headers .= "Bcc: ".$bcc."\r\n" ;
	$headers .= 'X-Mailer: PHP/' . phpversion();
	
	$verif_envoi_mail = TRUE;
	
	switch ($type) {
		
		default : 
		
			$content = "";
			$content .= "<html> \n";
			$content .= "<head> \n";
			$content .= "<title></title> \n";
			$content .= "</head> \n";
			$content .= '<body style="margin:0;padding:0;color:#000000;background-color:#f0f0f0;" bgcolor="#f0f0f0">';
			$content .= '<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="margin:0;padding:0;color:#000000;background-color:#f0f0f0;">
				<tr>
					<td valign="top" style="color:#000000;background:#f0f0f0;">
						<!-- Header -->
						<table class="mobile-wide" width="530" align="center" cellpadding="0" cellspacing="0" border="0" style="color:#000000;background:#ffffff;text-align:left;font-size:13px;line-height:19px;font-family:Helvetica, Arial, Verdana;">
							<!-- Padding -->
							<tr>
								<td colspan="3" bgcolor="#f0f0f0"><img class="mobile-hide" style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<!-- End Padding -->
							
							<!-- Layout Setup & Corners Above -->
							<tr>
								<td width="15" style="font-size:0px;line-height:0px;"><img class="mobile-hide" style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-above-left.png" alt=""></td>
								<td class="mobile-wide" width="500" style="font-size:0px;line-height:0px;"><img class="mobile-wide" style="display:block" width="500" height="15" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td width="15" style="font-size:0px;line-height:0px;"><img class="mobile-hide" style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-above-right.png" alt=""></td>
							</tr>
							<!-- End Layout Setup & Corners Above -->
							
							<!-- Logo & Meta -->
							<tr valign="top">
								<td rowspan="3" width="15" style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td style="font-size:0px;line-height:0px;"><img width="10" height="12" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt="" style="display:block;"></td>
								<td rowspan="3" width="15" style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<tr valign="top">
								<td><img width="138" height="25" src="'.ASSETS_URL.'mail/deloitte.png" alt="Deloitte" style="display:block;"></td>
							</tr>
							<tr valign="top">
								<td><img width="10" height="25" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt="" style="display:block;"></td>
							</tr>
							<tr valign="top">
								<td colspan="3" height="9" style="font-size:0px;line-height:0px;"><img class="mobile-wide" width="530" height="9" src="'.ASSETS_URL.'mail/header-divider.png" alt="" style="display:block;"></td>
							</tr>
							<tr valign="top">
								<td><br></td>
								<td style="color:#494949;">Deloitte France &nbsp;|&nbsp; '.$titre.'</td>
								<td><br></td>
							</tr>
							<tr>
								<td colspan="3" style="font-size:0px;line-height:0px;"><img style="display:block" width="10" height="7" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<!-- End Logo & Meta -->
			
							<!-- Hero Image -->
							<tr>
								<td colspan="3" bgcolor="#f0f0f0" style="font-size:0px;line-height:0px;"><img class="mobile-scale" style="display:block" width="530" height="165" src="'.ASSETS_URL.'mail/header.jpg" alt=""></td>
							</tr>
							<!-- End Hero Image -->
						</table>
						<!-- End Header -->
						
						<!-- Title -->
						<table class="mobile-wide" width="530" align="center" cellpadding="0" cellspacing="0" border="0" style="color:#313131;background:#ffffff;text-align:left;font-size:13px;line-height:19px;font-family:Helvetica, Arial, Verdana;">
							<!-- Layout Setup & Padding Above -->
							<tr>
								<td width="15" style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="12" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td class="mobile-wide" width="500" style="font-size:0px;line-height:0px;"><img class="mobile-wide" style="display:block" width="500" height="2" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td width="15" style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="2" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<!-- End Layout Setup & Padding Above -->
							
							<!-- Text -->
							<tr>
								<td><img width="10" height="10" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt="" style="display:block;"></td>
								<td valign="top">
									<span style="color:#002776;font-size:26px;line-height:32px;">'.$titre.'</span><br>
									<span style="color:#81bc00;font-size:26px;line-height:32px;">'.$soustitre.'</span><br>
							  </td>
								<td><img width="10" height="10" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt="" style="display:block;"></td>
							</tr>
							<!-- End Text -->
							
							<!-- Corners Below -->
							<tr>
								<td style="font-size:0px;line-height:0px;"><img class="mobile-hide" style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-below-left.png" alt=""></td>
								<td style="font-size:0px;line-height:0px;"><img style="display:block" width="10" height="15" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td style="font-size:0px;line-height:0px;"><img class="mobile-hide" style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-below-right.png" alt=""></td>
							</tr>
							<!-- End Corners Below -->
							
							<!-- Shadow -->
							<tr>
								<td colspan="3" bgcolor="#f0f0f0" style="font-size:0px;line-height:0px;"><img style="display:block" width="10" height="10" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<!-- End Shadow -->
						</table>
						<!-- End Title -->
					</td>
				</tr>
			</table>
			<!-- End Wrapper for Header & Title -->
			
			<!-- Wrapper for White 2:1 Blue -->
			
			<!-- End Wrapper for White 2:1 Blue -->
			
			<!-- Wrapper for White 3:1 Padding -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="margin:0;padding:0;color:#000000;background-color:#f0f0f0;">
				<tr>
					<td class="mobile-padding" width="0"><br></td>
					<td valign="top">
						<!-- Text 3:1 Padding -->
						<table class="mobile-wide" width="530" align="center" cellpadding="0" cellspacing="0" border="0" style="color:#000000;background:#ffffff;text-align:left;font-size:13px;line-height:19px;font-family:Helvetica, Arial, Verdana;">
							<!-- Layout Setup & Corners Above -->
							<tr>
								<td width="15" style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-above-left.png" alt=""></td>
								<td class="mobile-wide" width="420" style="font-size:0px;line-height:0px;"><img class="mobile-wide" style="display:block" width="420" height="2" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td class="mobile-hide" width="80" style="font-size:0px;line-height:0px;"><img class="mobile-hide" style="display:block" width="80" height="2" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td width="15" style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-above-right.png" alt=""></td>
							</tr>
							<!-- End Layout Setup & Corners Above -->
							
							<!-- Columns -->
							<tr>
								<td valign="top"><a name="story2"><br></a></td>
								
								<!-- Left Column -->
								<td>
									'.$message.'
								<!-- End Left Column -->
								
								<td><br></td>
								<td><br></td>
							</tr>
							<!-- End Columns -->
							
							<!-- Corners Below -->
							<tr>
								<td style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-below-left.png" alt=""></td>
								<td style="font-size:0px;line-height:0px;"><img style="display:block" width="10" height="10" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td style="font-size:0px;line-height:0px;"><img style="display:block" width="10" height="10" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
								<td align="right" style="font-size:0px;line-height:0px;"><img style="display:block" width="15" height="15" src="'.ASSETS_URL.'mail/white-below-right.png" alt=""></td>
							</tr>
							<!-- End Corners Below -->
							
							<!-- Shadow -->
							<tr>
								<td colspan="4" bgcolor="#f0f0f0" style="font-size:0px;line-height:0px;"><img class="mobile-wide" style="display:block" width="530" height="10" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<!-- End Shadow -->
						</table>
						<!-- End Text 3:1 Padding -->
					</td>
					<td class="mobile-padding" width="0"><br></td>
				</tr>
			</table>
			<!-- End Wrapper for White 3:1 Padding -->
			
			<!-- Wrapper for Sharing -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="margin:0;padding:0;color:#000000;background-color:#f0f0f0;">
				<tr>
					<td class="mobile-padding" width="0"><br></td>
					<td valign="top" bgcolor="#f0f0f0">
						<!-- Toolbar & Sharing -->
						<table class="mobile-wide" width="530" align="center" cellpadding="0" cellspacing="0" border="0" style="color:#000000;background:#f0f0f0;text-align:left;font-size:11px;line-height:16px;font-family:Helvetica, Arial, Verdana;">
							<tr>
								<td colspan="8"><img style="display:block" width="10" height="10" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<tr>
								 
								<td class="mobile-share-item" width="50" align="center" valign="top"><a href="http://www.linkedin.com/company/deloitte-france" style="color:#000000;text-decoration:none;"><img style="display:block" width="30" height="30" src="'.ASSETS_URL.'mail/share/linkedin.png" alt="" title="LinkedIn"></a></td>
								<td class="mobile-share-item" width="50" align="center" valign="top"><a href="https://www.facebook.com/DeloitteFrance" style="color:#000000;text-decoration:none;"><img style="display:block" width="30" height="30" src="'.ASSETS_URL.'mail/share/facebook.png" alt="" title="Facebook"></a></td>
								<td class="mobile-share-item" width="50" align="center" valign="top"><a href="https://twitter.com/DeloitteFrance" style="color:#000000;text-decoration:none;"><img style="display:block" width="30" height="30" src="'.ASSETS_URL.'mail/share/twitter.png" alt="" title="Twitter"></a></td>
								<td class="mobile-share-item" width="50" align="center" valign="top"><a href="https://plus.google.com/104843964674437093322/posts" style="color:#000000;text-decoration:none;"><img style="display:block" width="30" height="30" src="'.ASSETS_URL.'mail/share/google-plus.png" alt="" title="Google+"></a></td>
								<td class="mobile-share-item" width="50" align="center" valign="top"><a href="https://www.youtube.com/user/deloittefrance" style="color:#000000;text-decoration:none;"><img style="display:block" width="30" height="30" src="'.ASSETS_URL.'mail/share/youtube.png" alt="" title="YouTube"></a></td>
								<td class="mobile-share-item" align="center"><br></td>
							</tr>
						</table>
						<!-- End Toolbar & Sharing -->
					</td>
					<td class="mobile-padding" width="0"><br></td>
				</tr>
			</table>
			<!-- End Wrapper for Sharing -->
			
			<!-- Wrapper for Footer -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="margin:0;padding:0;color:#000000;background-color:#f0f0f0;">
				<tr>
					<td class="mobile-padding" width="0"><br></td>
					<td valign="top" bgcolor="#f0f0f0">
						<!-- Footer -->
						<table class="mobile-wide" width="520" align="center" cellpadding="0" cellspacing="0" border="0" style="color:#000000;background:#f0f0f0;text-align:left;font-size:11px;font-family:Helvetica, Arial, Verdana;">
							<!-- Padding -->
							<tr>
								<td><img style="display:block" width="20" height="15" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<!-- End Padding -->
							
							<!-- Text -->
							<tr>
								<td class="mobile-wide" width="500">
									<a href="http://www.deloitte.com/" style="color:#00a1de;text-decoration:none;">Deloitte SAS</a><br>
									185 avenue Charles-de-Gaulle<br>
									92524 Neuilly-sur-Seine Cedex<br>
									France<br>
									<br>
									Deloitte fait référence à un ou plusieurs cabinets  membres de Deloitte Touche Tohmatsu Limited, société de droit anglais («  private company limited by guarantee »), et à son réseau de cabinets membres  constitués en entités indépendantes et juridiquement distinctes. Pour en savoir  plus sur la structure légale de Deloitte Touche Tohmatsu Limited et de ses  cabinets membres, consulter <a href="http://info.deloitte.fr/r/?F=ujdgbshyj3magez3dktjzmm7nh9knleuegaw6j9q9nmt4aryanjvmfa-5326529" target="_blank">www.deloitte.com/about</a>.  En France, Deloitte SAS est le cabinet membre de Deloitte Touche Tohmatsu  Limited, et les services professionnels sont rendus par ses filiales et ses  affiliés.<br>
									 
									<br>
									©'.date("Y").' Deloitte SAS<br>
								</td>
							</tr>
							<!-- End Text -->
							
							<!-- Padding -->
							<tr>
								<td><img style="display:block" width="20" height="15" src="'.ASSETS_URL.'mail/deloitte-gap.gif" alt=""></td>
							</tr>
							<!-- End Padding -->
						</table>
						<!-- End Footer -->
					</td>
					<td class="mobile-padding" width="0"><br></td>
				</tr>
			</table>
			';
			$content .= "</body> \n";
			$content .= "</html> \n";
		   
			$verif_envoi_mail = @mail ($mailing_list, $subject, $content, $headers);
			return 	$verif_envoi_mail;
	} // end switch;
}

?>